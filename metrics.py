import math


def cosine_similarity(dictionary1, dictionary2):
    intersection = set(dictionary1.keys()) & set(dictionary2.keys())
    numerator = sum([dictionary1[x] * dictionary2[x] for x in intersection])
    sum1 = sum([dictionary1[x] ** 2 for x in dictionary1.keys()])
    sum2 = sum([dictionary2[x] ** 2 for x in dictionary2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    if denominator == 0:
        return 0
    return float(numerator) / denominator


def dice_similarity(dictionary1, dictionary2):
    intersection = (set(dictionary1.keys()).intersection(dictionary2.keys()))

    if not intersection:
        return 0
    denominator = sum([dictionary1[word] + dictionary2[word] for word in intersection])
    if denominator == 0:
        return 0
    numerator = sum([2 * min(dictionary1[word], dictionary2[word]) for word in intersection])
    return numerator / denominator