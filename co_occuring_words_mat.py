import time
from datetime import timedelta, datetime
import calculations
import logs_mat
import metrics_mat

import words

unique_words, paragraphs = words.read_paragraphs()
word_count = len(unique_words)

EXECUTION_NUMBER = '18'
start_time = time.time()


def calculate_t():
    probability_matrix = [[0 for x in range(word_count)] for x in range(word_count)]

    word_probability, word_paragraphs = calculations.pre_calculations(paragraphs, unique_words)
    for i in range(0, len(unique_words)):
        for j in range(0, len(unique_words)):
            if i > j:
                number_of_appearances_together = len(
                    [x for x in word_paragraphs[unique_words[i]] if x in word_paragraphs[unique_words[j]]])
                if number_of_appearances_together > 3:
                    probability_matrix[i][j] = number_of_appearances_together
                    probability_matrix[j][i] = number_of_appearances_together

    return probability_matrix


def main():
    probability_matrix = calculate_t()
    logs_mat.log_matrix(probability_matrix, EXECUTION_NUMBER, unique_words)
    logs_mat.log_values(probability_matrix, EXECUTION_NUMBER, unique_words)
    total_iterations_left = word_count * (word_count - 1) / 2

    print("t time")
    print("%s" % (time.time() - start_time))
    print(str(word_count) + " UNIQUE WORDS")
    print(str(total_iterations_left) + " ITERATIONS")

    result_file = open('output/output-' + EXECUTION_NUMBER + '.txt', 'w', encoding='utf-8')
    similarity_matrix = [[0 for x in range(word_count)] for x in range(word_count)]

    it_time = time.time()
    it_count = 0
    for i in range(0, len(unique_words)):
        for j in range(0, len(unique_words)):
            if i > j:
                similarity = metrics_mat.dice_similarity(probability_matrix[i], probability_matrix[j], unique_words)
                # similarity = metrics_mat.cosine_similarity(global_mat[i], global_mat[j])
                similarity_matrix[i][j] = similarity
                similarity_matrix[j][i] = similarity

                if it_count % 10000 == 0:
                    time_for_10000_it = (time.time() - it_time)
                    approx_seconds_left = total_iterations_left/10000 * (time_for_10000_it)
                    print("ITERATION", it_count)
                    print("APPROX", approx_seconds_left, "SECONDS LEFT")
                    sec = timedelta(seconds=int(approx_seconds_left))
                    d = datetime(1, 1, 1) + sec
                    print("\t", d.day-1, "days", d.hour, "hours", d.minute, "minutes", d.second, "seconds")
                    it_time = time.time()
                it_count += 1
                total_iterations_left -= 1

    for i in range(0, len(unique_words)):
        top_values = sorted(zip(similarity_matrix[i], unique_words), reverse=True)[:3]
        result_file.write(unique_words[i] + "\n")
        result_file.write(str(top_values))
        result_file.write("\n\n")


words.write_words("output/bag-of-words.txt", sorted(unique_words))
main()
print("%s" % (time.time() - start_time))
