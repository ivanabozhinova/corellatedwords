import os
import string
import re


def read_words(file_name):
    doc = open(file_name, encoding="utf-8")
    word_list = []
    for line in doc:
        line = line.strip()
        word_list.append(line)
    word_list = word_list[1:]
    return word_list


stop_words = read_words('input/stopwords.txt')
no_words = ['\n', '\t', '']
articles = ('та', 'то', 'от', 'те',)
articles_postfix = ['-от', '-oт']


# get the list of character we want stripped off
filter_chars = set(string.punctuation)
filter_chars.add('\n')
filter_chars.add('„')
filter_chars.add('“')


def is_macedonian(word):
    for letter in word.lower():
        if letter in list(string.ascii_lowercase):
            return False
    return True


def is_stop_word(word):
    return word in stop_words or word in no_words


def filter_word(word):
    word = ''.join(ch for ch in word if ch not in filter_chars or ch == '-')
    word = word.lower()
    if word.endswith(tuple(articles_postfix)):
        word = word.split('-')[0]
    if '﻿' in word:
        word = word[1:]
    if word == '-':
        return ''
    return word


def filter_stop_words(words):
    result = []
    for word in words:
        word = filter_word(word)
        if not is_stop_word(word):
            result.append(word)

    return result


def valid_if_number(word):
    if word.isdigit():
        if not 1500 < int(word) < 2050:
            return False
    return True


def read_dictionary(index):
    dictionary_words = {}
    dictionary_lines = read_words('input/wfl-mk.txt')
    for line in dictionary_lines:
        parts = line.split()
        dictionary_words[parts[0]] = parts[index]
    return dictionary_words


dictionary_tags = read_dictionary(2)
dictionary_stems = read_dictionary(1)


def is_century(word):
    century_letters = ["x", "i", "v"]
    for letter in word.lower():
        if letter not in century_letters:
            return False
    return True


def filter_unnecessary_words(word_list):
    excluded_tags = ["I", "Q", "S", "C", "P"]
    result = []
    for word in word_list:
        word = word.strip()
        if is_macedonian(word) and not is_century(
                word) and len(word) > 1 \
                and valid_if_number(word) and not word.endswith('-') and not word.startswith('-') and '‐' not in word \
                and not any(char.isdigit() for char in word):
            if word in dictionary_tags.keys():
                if dictionary_tags[word][:1] not in excluded_tags:
                    result.append(word)
                    # print(word, dictionary_tags[word])
            else:
                result.append(word)

    return result


def find_stems(words_in_paragraph):
    words_in_paragraph_stemmed = []
    for word in words_in_paragraph:
        if word in dictionary_stems.keys():
            words_in_paragraph_stemmed.append(dictionary_stems[word])
        else:
            words_in_paragraph_stemmed.append(word)
    return words_in_paragraph_stemmed


def read_paragraphs():
    unique_words = set()
    all_files = os.listdir('input/corpus')
    paragraphs_list = []
    for file in all_files:
        doc_file = open('input/corpus/' + file, encoding="utf-8")
        for paragraph in doc_file:
            words_in_paragraph = filter_stop_words(paragraph.split())
            words_in_paragraph = filter_unnecessary_words(words_in_paragraph)
            #words_in_paragraph_stemmed = find_stems(words_in_paragraph)
            if words_in_paragraph:
                paragraphs_list.append(words_in_paragraph)
                unique_words = unique_words | set(words_in_paragraph)
    # return paragraphs_list, list(unique_words)
    return list(unique_words), paragraphs_list


def write_words(file_name, word_list):
    file = open(file_name, "w", encoding="utf-8")
    for word in word_list:
        file.write(word + '\n')
