def count_appearances(word, paragraphs):
    count = 0
    for paragraph in paragraphs:
        if word in paragraph:
            count += 1
    return count


def pre_calculations(paragraphs, unique_words):
    m = len(paragraphs)
    word_probability = {}
    word_paragraphs = {}
    for word in unique_words:
        pars_for_word = []
        word_probability[word] = count_appearances(word, paragraphs) / m
        for paragraph in paragraphs:
            if word in paragraph:
                pars_for_word.append(paragraph)
        word_paragraphs[word] = pars_for_word

    return word_probability, word_paragraphs


