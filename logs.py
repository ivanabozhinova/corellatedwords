def log_matrix(global_map, execution_number):
    file_co_occurring_dict = open('logs/' + execution_number + '-log-mat.txt', 'w', encoding='utf-8')
    for key in sorted(global_map.keys()):
        file_co_occurring_dict.write(key + "\t" + str((global_map[key])) + "\n")


def log_values(global_map, execution_number):
    file_co_occurring_values = open('logs/' + execution_number + '-log-val.txt', 'w', encoding='utf-8')
    for key in sorted(global_map.keys()):
        file_co_occurring_values.write(key + "\t")
        for key2 in global_map[key]:
            file_co_occurring_values.write(str(global_map[key][key2]) + "\t")
        file_co_occurring_values.write("\n")
        file_co_occurring_values.write("\n")


def log_t_test(global_map, execution_number):
    file_co_occurring_t = open('logs/' + execution_number + '-t-test.txt', 'w', encoding='utf-8')
    for key in sorted(global_map.keys()):
        for key2 in global_map[key]:
            file_co_occurring_t.write(key + "\t" + key2 + "\t" + str(global_map[key][key2]) + "\n")


def open_log_t_test(execution_number):
    global file_co_occurring_t
    file_co_occurring_t = open('logs/' + execution_number + '-t-test.txt', 'w', encoding='utf-8')
    file_co_occurring_t.write("WORD1" + "\t" + "WORD2" + "\t")
    file_co_occurring_t.write("W1 PROBABILITY" + "\t" + "W2 PROBABILITY" + "\t")
    file_co_occurring_t.write("W1&W2 PROBABILITY" + "\t")

    file_co_occurring_t.write("W1 COUNT" + "\t" + "W2 COUNT" + "\t")
    file_co_occurring_t.write("W1&W2 COUNT" + "\t")
    
    file_co_occurring_t.write("#PARAGRAPHS" + "\t")
    file_co_occurring_t.write("T VALUE" + "\n")


def log_t_value(unique_word_i, unique_word_j, word_probability_i,   word_probability_j, probability_couple,
                word_count_i, word_count_j, count_couple, m, t):
    file_co_occurring_t.write(unique_word_i + "\t" + unique_word_j + "\t")
    file_co_occurring_t.write(str(word_probability_i) + "\t" + str(word_probability_j) + "\t")
    file_co_occurring_t.write(str(probability_couple) + "\t")
    file_co_occurring_t.write(str(word_count_i) + "\t" + str(word_count_j) + "\t")
    file_co_occurring_t.write(str(count_couple) + "\t")
    file_co_occurring_t.write(str(m) + "\t")
    file_co_occurring_t.write(str(t) + "\n")