import math
import operator
import time
import calculations
import logs
import metrics

import words


EXECUTION_NUMBER = '09'
start_time = time.time()


# funkcijata vraka
# dictionary koe gi sozdri site mozni zborovi kako klucevi
# vrednost za sekoj zbor e nov dictionary,
# koj povtorno sodrzi zborovi kako klucevi,
# a vrednosti se verojatnostite(t-test) tie dva zbora da se pojavuvaat vo ista okolina (prozorec)
def calculate_t():
    logs.open_log_t_test(EXECUTION_NUMBER)
    # rezultatot koj se vraka
    global_map = {}

    # unique_words e lista od site mozni isfiltrirani zborovi
    # paragraphs e lista od paragrafi,
    # sekoj paragraf e lista od zborovi
    unique_words, paragraphs = words.read_paragraphs()


    # bidejki stedime vreme, a ne memorija gi cuvame ovie
    # word_probability e dictionary so kluc zbor, a vrednost e verojatnosta da se sretne toj zbor
    # word_paragraphs e dictionary so kluc zbor, a vrednost lista od paragrafite vo koi toj zbor se pojavuva
    word_probability, word_paragraphs = calculations.pre_calculations(paragraphs, unique_words)

    m = len(paragraphs)

    # se iterira niz sekoj mozen zbor
    for i in range(0, len(unique_words)):
        #inicijalizacija na dictionary-to sto ke bide vrednost vo global_map
        word_map = {}
        # se iterira niz sekoj mozen zbor
        for j in range(0, len(unique_words)):
            if not i == j:
                # verojatnosta dvata zbora unique_words[i] i unique_words[j] da se pojavat zaedno
                # e ednakva na brojot na paragrafi vo koi tie se pojavuvaat zaaedno
                # podelen so vkupniot broj paragrafi

                number_of_appearances_together = len([x for x in word_paragraphs[unique_words[i]] if x in word_paragraphs[unique_words[j]]])
                probability_couple = number_of_appearances_together / m

                # po formulata za t test

                variance = word_probability[unique_words[i]] * word_probability[unique_words[j]]
                t = (probability_couple - variance) / math.sqrt(variance)

                # ako t e 0 ne treba da go cuvame
                logs.log_t_value(unique_words[i], unique_words[j], word_probability[unique_words[i]],
                                     word_probability[unique_words[j]], probability_couple,
                                     calculations.count_appearances(unique_words[i], paragraphs),
                                     calculations.count_appearances(unique_words[j], paragraphs),
                                     number_of_appearances_together,
                                     m, t)
                #word_map[unique_words[j]] = t
                word_map[unique_words[j]] = number_of_appearances_together

            global_map[unique_words[i]] = word_map


    return global_map, unique_words


def main():
    global_map, unique_words = calculate_t()

    # se zacuvuvaat dictionariata i vrednostite, ako ni pritreba za vizuelizacija
    logs.log_matrix(global_map, EXECUTION_NUMBER)
    logs.log_values(global_map, EXECUTION_NUMBER)

    print("t time")
    print("%s" % (time.time() - start_time))
    print(str(len(unique_words)) + " UNIQUE WORDS")
    print(str(len(unique_words) * len(unique_words)) + " ITERATIONS")

    result_file = open('output/output-' + EXECUTION_NUMBER + '.txt', 'w', encoding='utf-8')


    it_time = time.time()
    it_count = 0
    # se iterira niz sekoj mozen zbor
    for i in range(0, len(unique_words)):
        # za sekoj zbor povtorno se cuva dicitionary
        # key vo ovoj dictionary e pak zbor
        # value e vrednosta za SLICNOST
        dict_for_word = {}
        for j in range(0, len(unique_words)):
            if not i == j:
                similarity = metrics.dice_similarity(global_map[unique_words[i]], global_map[unique_words[j]])
                dict_for_word[unique_words[j]] = similarity
                if it_count % 1000 == 0:
                    print("ITERATION " + str(it_count) + "\t" + str(time.time() - it_time))
                    it_time = time.time()
                it_count += 1

        # dict_for_word se sortira po VREDNOST i se vrakaat najvisokite 3
        top_values = dict(sorted(dict_for_word.items(), key=operator.itemgetter(1), reverse=True)[:3])

        # se zapisuvaat vo fajlot za rezultati
        result_file.write(unique_words[i])
        for key in top_values:
            result_file.write("\n" + key + "\t" + str(top_values[key]))
        result_file.write("\n\n")


main()
print("%s" % (time.time() - start_time))

