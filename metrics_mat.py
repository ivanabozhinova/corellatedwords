import math


def cosine_similarity(array1, array2):
    numerator = sum([array1[x] * array2[x] for x in range(len(array1))])
    sum1 = sum([array1[x] ** 2 for x in range(len(array1))])
    sum2 = sum([array2[x] ** 2 for x in range(len(array2))])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    if denominator == 0:
        return 0
    return float(numerator) / denominator


def dice_similarity(array1, array2, unique_words):
    denominator = sum([array1[i] + array2[i] for i in range(len(unique_words))])
    if denominator == 0:
        return 0
    numerator = sum([2 * min(array1[i], array2[i]) for i in range(len(unique_words))])
    return numerator / denominator