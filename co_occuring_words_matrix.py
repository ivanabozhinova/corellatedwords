import math
import words
import time
import operator

EXECUTION_NUMBER = '03'

start_time = time.time()



def pre_calculations(paragraphs, unique_words):
    M = len(paragraphs)
    word_probability = {}
    word_paragraphs = {}
    for word in unique_words:
        pars_for_word = []
        word_probability[word] = count_appearances(word, paragraphs) / M
        for paragraph in paragraphs:
            if word in paragraph:
                pars_for_word.append(paragraph)
        word_paragraphs[word] = pars_for_word

    return word_probability, word_paragraphs


def count_appearances(word, paragraphs):
    count = 0
    for paragraph in paragraphs:
        if word in paragraph:
            count += 1
    return count


def calculate_t():
    unique_words, paragraphs = words.read_paragraphs()

    word_count = len(unique_words)

    t_matrix = [[0 for x in range(word_count)] for x in range(word_count)]

    word_probability, word_paragraphs = pre_calculations(paragraphs, unique_words)

    M = len(paragraphs)
    count = 1
    for i in range(0, word_count):
        for j in range(0, word_count):
            if i > j:
                count += 1

                probability_couple = len(
                    [x for x in word_paragraphs[unique_words[i]] if x in word_paragraphs[unique_words[j]]]) / M

                if probability_couple > 0:
                    t = (probability_couple - word_probability[unique_words[i]] * word_probability[
                        unique_words[j]]) / math.sqrt(probability_couple / M)
                    t_matrix[i][j] = t
                    t_matrix[j][i] = t

    print(count, "ITERATIONS")
    print(len(unique_words), "UNIQUE WORDS")
    return t_matrix, unique_words


def calculate_similarity(array_word1, array_word2, word_count):
    denominator = sum([array_word1[i] + array_word2[i] for i in range(word_count)])
    #denominator = sum(array_word1[:] + array_word2[:])
    # denominator = sum(itertools.chain(array_word1, array_word2))
    if denominator == 0:
        return 0
    numerator = sum([2 * min(array_word1[i], array_word2[i]) for i in range(word_count)])
    #numerator = 2 * sum(map(min, zip(array_word1, array_word2)))
    return numerator / denominator


def parallel_test():
    # def worker_init(matrix):
    # global worker_matrix
    #     worker_matrix = matrix
    #
    #
    # def worker(i, j):
    #     similarity = calculate_similarity(worker_matrix[i], worker_matrix[j])
    #     return i, j, similarity
    #
    #
    # def main(matrix):
    #     size = len(matrix)
    #     result = [[0] * size for _ in range(size)]
    #     with multiprocessing.Pool(initializer=worker_init, initargs=(matrix,)) as pool:
    #         for i, j, val in pool.starmap(worker, itertools.combinations(range(size), 2)):
    #             result[i][j] = result[j][i] = val
    #     return result

    pass


def measure_similarity():
    t_matrix, unique_words = calculate_t()
    print("T MATRIX: %s" % (time.time() - start_time))
    word_count = len(unique_words)
    similarity_matrix = [[0 for x in range(word_count)] for x in range(word_count)]

    count = 1
    it_time = time.time()
    it_time1 = time.time()

    result_file = open('output/output-' + EXECUTION_NUMBER + '.txt', 'w', encoding='utf-8')

    for i in range(word_count):
        result_file.write(unique_words[i])
        for j in range(word_count):
            if i > j:
                count += 1
                similarity = calculate_similarity(t_matrix[i], t_matrix[j], word_count)
                similarity_matrix[i][j] = similarity
                similarity_matrix[j][i] = similarity

        word_map = {}
        values = similarity_matrix[i]
        for j in range(0, word_count):
            word_map[unique_words[j]] = values[j]

        top_values = dict(sorted(word_map.items(), key=operator.itemgetter(1), reverse=True)[:3])

        for key in top_values:
            result_file.write("\n" + key + "\t" + str(top_values[key]))
        result_file.write("\n\n")

    print("SIMILARITY MATRIX: %s" % (time.time() - it_time))



measure_similarity()